package aolmedo.utility.email.executables;

import aolmedo.utility.email.EML;
import aolmedo.utility.email.ExecutionMessages;
import java.io.File;

public class Splitter {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    String filename = args[0];
    try {
      EML eml = EML.createFromExisting(filename);
      String path = args[1] + File.separator;
      EML.split(path, eml);
      ExecutionMessages.successMessage();
    } catch (Exception ex) {
      ExecutionMessages.errorMessage();
    }
  }

}
