package aolmedo.utility.email.executables;

import aolmedo.utility.email.EML;
import aolmedo.utility.email.ExecutionMessages;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

public class Info {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    String filename = args[0];
    try {
      JsonObjectBuilder job = Json.createObjectBuilder();
      EML eml = EML.createFromExisting(filename);
      JsonArrayBuilder jab = Json.createArrayBuilder();
      for (String recipient : eml.getRecipients()) {
        jab.add(Json.createObjectBuilder().add("email", recipient));
      }
      
      JsonArrayBuilder jab2 = Json.createArrayBuilder();
      for (String recipient : eml.getRecipientsBcc()) {
        jab2.add(Json.createObjectBuilder().add("email", recipient));
      }

      job.add("recipients", jab)
              .add("recipientsBcc", jab2)
              .add("subject", eml.getSubject())
              .add("from", eml.getFrom());
      System.out.println(job.build());
    } catch (Exception ex) {
      ExecutionMessages.errorMessage();
    }
  }
}
