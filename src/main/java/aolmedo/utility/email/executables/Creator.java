package aolmedo.utility.email.executables;

import aolmedo.utility.email.ExecutionMessages;
import aolmedo.utility.email.EML;
import java.nio.charset.StandardCharsets;

public class Creator {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    try {
      String subject = new String(args[1].getBytes(), StandardCharsets.UTF_8);
      EML eml = new EML(
              subject,
              args[2],
              args[3],
              args[4],
              args.length > 5 ? args[5] : "",
              args.length > 6 ? args[6] : ""
      );
      eml.create(args[0]);
      ExecutionMessages.successMessage();
    } catch (Exception ex) {
      ExecutionMessages.errorMessage();
    }
  }

}
