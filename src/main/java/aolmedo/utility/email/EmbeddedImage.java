/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aolmedo.utility.email;

/**
 *
 * @author AOlmedo
 */
public class EmbeddedImage {
    
    private String cid;
    private String base64data;
    private String mimeType;
    
    /**
     * Crea un objeto de tipo EmbeddedImage utilizado para las imágenes
     * embebidas de los correos.
     * 
     * @param cid Identificador de la imágen, debe ser único
     * @param base64data La imagen codificada en base64
     * @param mimeType la extensión y tipo del fichero (image/png)
     */
    public EmbeddedImage(String cid, String base64data, String mimeType) {
        this.cid = cid;
        this.base64data = base64data;
        this.mimeType = mimeType;
    }
    
    /**
     * Devuelve la variable cid
     * 
     * @return 
     */
    public String getCid() {
        return this.cid;
    }
    
    /**
     * Actualiza la variable cid
     * 
     * @param cid 
     */
    public void setCid(String cid) {
        this.cid = cid;
    }
    
    /**
     * Devuelve la variable base64data
     * 
     * @return 
     */
    public String getBase64data() {
        return this.base64data;
    }
    
    /**
     * Actualiza la variable base64data
     * 
     * @param base64data 
     */
    public void setBase64data(String base64data) {
        this.base64data = base64data;
    }
    
    /**
     * Devuelve la variable mimeType
     * 
     * @return
     */
    public String getMimeType() {
        return this.mimeType;
    }
    
    /**
     * Actualiza la variable mimeType
     * 
     * @param mimeType 
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
