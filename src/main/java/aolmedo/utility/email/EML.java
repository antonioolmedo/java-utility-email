package aolmedo.utility.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;

public class EML {

  private static final String BASE64_PATTERN = "(data:(image\\/[a-z]+);base64,([^\"]*))";

  private String html;
  private String plainText;
  private List<String> recipients = new ArrayList<>();
  private List<String> recipientsBcc = new ArrayList<>();
  private String subject;
  private String from;
  private List<Attachment> attachments = new ArrayList<>();
  private List<EmbeddedImage> embeddedImages = new ArrayList<>();
  private MimeMessage message;

  /**
   * Crea un objeto EML leyendolo de un fichero
   *
   * @param filename
   * @return
   * @throws FileNotFoundException
   * @throws MessagingException
   * @throws IOException
   */
  public static EML createFromExisting(String filename) throws FileNotFoundException, FileNotFoundException, MessagingException, IOException {
    InputStream source = new FileInputStream(new File(filename));
    // Creamos un MimeMessage
    Properties properties = System.getProperties();
    Session session = Session.getDefaultInstance(properties);
    MimeMessage message = new MimeMessage(session, source);

    EML eml = new EML();
    eml.setMessage(message);
    eml.setRecipients(EML.readRecipientsFromMessage(message, Message.RecipientType.TO));
    eml.setRecipientsBcc(EML.readRecipientsFromMessage(message, Message.RecipientType.BCC));
    eml.setFrom(message.getFrom()[0].toString());
    eml.setSubject(message.getSubject());
    EML.handleContent(eml, (MimeMultipart) message.getContent());
    return eml;
  }

  /**
   * Constructor vacío
   */
  private EML() {
  }

  /**
   * Constructor
   *
   * @param subject       Asunto
   * @param from          Remitente
   * @param to            Destinatarios
   * @param bodyPath      Localización del fichero que contiene el html
   * @param attachments   Localización de los ficheros para adjuntar
   * @param recipientsBcc Destinatarios con copia oculta
   * @throws IOException
   */
  public EML(String subject, String from, String to, String bodyPath, String attachments, String recipientsBcc) throws IOException {
    this.html = EML.getHtmlFromFile(EML.getFilePath(bodyPath));
    this.fillEmbeddedImages();
    this.recipients = EML.parseElements(to);
    this.subject = subject;
    this.from = from;
    List<String> attachsFullPaths = attachments.compareToIgnoreCase("") == 0 ? new ArrayList() : EML.parseElements(attachments);
    this.attachments = Attachment.createArrayListFromArrayString(attachsFullPaths);
    this.recipientsBcc = recipientsBcc.compareToIgnoreCase("") == 0 ? new ArrayList() : EML.parseElements(recipientsBcc);

    // Creamos un MimeMessage
    Properties properties = System.getProperties();
    Session session = Session.getDefaultInstance(properties);
    this.message = new MimeMessage(session);
  }

  /**
   * Crea el fichero en disco
   *
   * @param filename path y nombre del fichero sin extensión a escribir
   * @throws MessagingException
   * @throws AddressException
   * @throws FileNotFoundException
   * @throws IOException
   */
  public void create(String filename) throws MessagingException, AddressException, FileNotFoundException, IOException, Exception {
    // Añadimos el sujeto
    this.message.setSubject(this.subject, "utf-8");
    
    // Añadimos el remitente
    this.message.setFrom(new InternetAddress(this.from));

    //  Añadimos los destinatarios al correo
    for (String recipient : this.recipients) {
      this.message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
    }
    
    //  Añadimos los destinatarios con copia oculta al correo
    for (String recipient : this.recipientsBcc) {
      this.message.addRecipient(Message.RecipientType.BCC, new InternetAddress(recipient));
    }

    // Creamos el contenido del mensaje
    MimeBodyPart content = new MimeBodyPart();
    
    // Rellenamos el mensaje con html si lo tenemos, si no con texto plano
    if (this.html != null) {
      content.setText(this.html, "utf-8", "html");
    } else if(this.plainText != null) {
      content.setText(this.plainText, "utf-8", "plain");
    } else {
      content.setText("", "utf-8", "plain");
    }
    
    Multipart multipartRelated = new MimeMultipart("related");
    multipartRelated.addBodyPart(content);
    
     //  añadimos las imágenes
    this.addEmbeddedImages(multipartRelated);
    
    //  raiz del mensaje para formarlo bien como message/rfc822
    Multipart multipart = new MimeMultipart();
    MimeBodyPart primaryContent = new MimeBodyPart();
    primaryContent.setContent(multipartRelated);
    multipart.addBodyPart(primaryContent);
    
     //  añadimos los ficheros
    this.addAttachments(multipart);

    // Añadimos todo el contenido al mensaje
    message.setContent(multipart);

    //  Guardamos el fichero
    message.writeTo(new FileOutputStream(new File(filename + ".eml")));
  }

  /**
   * Lee el EML y guarda en el directorio indicado los adjuntos y el body.html
   *
   * @param path
   * @param eml
   * @throws UnsupportedEncodingException
   * @throws FileNotFoundException
   * @throws IOException
   * @throws Exception
   */
  public static void split(String path, EML eml) throws UnsupportedEncodingException, FileNotFoundException, IOException, Exception {
    //  escribimos los adjuntos
    for (Attachment attch : eml.getAttachments()) {
      attch.setDir(path);
      File f = new File(attch.getFullPath());
      FileOutputStream fos = new FileOutputStream(f);
      byte[] buf = new byte[4096];
      int bytesRead;
      while ((bytesRead = attch.getContent().read(buf)) != -1) {
        fos.write(buf, 0, bytesRead);
      }
      fos.close();
    }

    //  reemplazamos el html por las imagenes
    for (EmbeddedImage embeddedImage : eml.getEmbeddedImages()) {
      eml.setHtml(eml.getHtml().replace("cid:" + embeddedImage.getCid(), "data:" + embeddedImage.getMimeType() + ";base64," + embeddedImage.getBase64data()));
    }

    //  escribimos el body.html
    byte[] buffer = eml.getHtml().getBytes("utf-8");
    File bf = new File(path + "body.html");
    FileOutputStream fos = new FileOutputStream(bf);
    fos.write(buffer);
    fos.close();
  }

  /**
   * Añade a las partes del contenido del mensaje las imágenes embebidas
   *
   * @param multipart
   * @throws MessagingException
   * @throws IOException
   */
  private void addEmbeddedImages(Multipart multipart) throws MessagingException, IOException {
    for (EmbeddedImage ei : this.embeddedImages) {
      byte[] imgBytes = Base64.getDecoder().decode(ei.getBase64data());
      MimeBodyPart attachment = new MimeBodyPart();
      DataSource source = new ByteArrayDataSource(imgBytes, ei.getMimeType());
      attachment.setDataHandler(new DataHandler(source));
      attachment.setContentID("<" + ei.getCid() + ">");
      attachment.setDisposition(BodyPart.INLINE);
      multipart.addBodyPart(attachment);
    }
  }

  /**
   * Añade a las partes del contenido del mensaje los ficheros adjuntos
   *
   * @param multipart
   * @throws MessagingException
   * @throws IOException
   */
  private void addAttachments(Multipart multipart) throws MessagingException, IOException, Exception {
    for (Attachment attachment : this.attachments) {
      File file = new File(attachment.getFullPath());
      MimeBodyPart mbp = new MimeBodyPart();
      DataSource source = new FileDataSource(file);
      mbp.setDataHandler(new DataHandler(source));
      mbp.setFileName(file.getName());
      mbp.addHeader("Content-Disposition", "attachment");
      multipart.addBodyPart(mbp);
    }
  }

  /**
   * Obtiene el contenido de un fichero y actualiza la variable html
   *
   * @param path
   * @throws IOException
   */
  private static String getHtmlFromFile(Path path) throws IOException {
    String html = "";
    List<String> lines = Files.readAllLines(path);
    Iterator i = lines.iterator();
    while (i.hasNext()) {
      html = html + (String) i.next();
    }
    return html;
  }

  /**
   * Devuelve el Path de un filename
   *
   * @param path localización del fichero
   * @return
   */
  private static Path getFilePath(String path) {
    return new File(path).toPath();
  }

  /**
   * Devuelve un array con los elementos, limpia todos los espacios y separa los
   * elementos por ",".
   *
   * @param elements Cadena de texto con los elemntos separados por ","
   * @return
   */
  private static List<String> parseElements(String elements) {
    return new ArrayList<>(Arrays.asList(elements.trim().split("\\s?,\\s?")));
  }

  /**
   * Obtiene las imágenes embebidas en base64 del html
   */
  private void fillEmbeddedImages() {
    this.embeddedImages = new ArrayList<>();
    Matcher matcher = Pattern.compile(EML.BASE64_PATTERN).matcher(html);
    while (matcher.find()) {
      String cid = UUID.randomUUID().toString();
      String mimeType = matcher.group(2);
      String base64data = matcher.group(3);
      EmbeddedImage ei = new EmbeddedImage(cid, base64data, mimeType);

      this.html = this.html.replace(matcher.group(1), "cid:" + ei.getCid());
      this.embeddedImages.add(ei);
    }
  }

  /**
   * Devuelve los destinatarios de un tipo del mensaje
   *
   * @param message
   * @param type
   * @return
   * @throws MessagingException
   */
  private static List<String> readRecipientsFromMessage(MimeMessage message, RecipientType type) throws MessagingException {
    List<String> recipients = new ArrayList<>();
    Address[] addresses = message.getRecipients(type);
    if (addresses != null) {
      for (Address addresse : addresses) {
        recipients.add(addresse.toString());
      }
    }
    return recipients;
  }

  /**
   * Lee las partes del cuerpo del EML
   *
   * @param eml
   * @param content
   * @throws MessagingException
   * @throws IOException
   */
  private static void handleContent(EML eml, MimeMultipart content) throws MessagingException, IOException {
    for (int i = 0; i < content.getCount(); i++) {
      BodyPart bp = content.getBodyPart(i);
      handleBodyPart(eml, bp);
    }
  }

  /**
   * Lee la parte del cuerpo y comprueba de que tipo es para poder sacar toda la
   * información
   *
   * @param eml
   * @param bp
   * @throws MessagingException
   * @throws IOException
   */
  private static void handleBodyPart(EML eml, BodyPart bp) throws MessagingException, IOException {
    //  comprobamos si es una imagen
    String cid = null, mimeType = null, base64data = null;
    Enumeration headers = bp.getAllHeaders();// recorremos los headers
    if (bp.getContentType().contains("image")) {//  solo para imagenes
      while (headers.hasMoreElements()) {// si rellena todos los datos anteriores es que es embebida
        Header header = (Header) headers.nextElement();
        if (header.getName().compareToIgnoreCase("Content-ID") == 0) {
          cid = header.getValue().replace("<", "").replace(">", "");
          base64data = Base64.getEncoder().encodeToString(IOUtils.toByteArray((InputStream) bp.getContent()));
        } else if (header.getName().compareToIgnoreCase("Content-Type") == 0) {
          String[] mime = header.getValue().split(";");
          mimeType = mime.length == 2 ? mime[0] + ";" : header.getValue();
        }
      }
    }
    
    if (cid != null && mimeType != null && base64data != null) {
      eml.getEmbeddedImages().add(new EmbeddedImage(cid, base64data, mimeType));
    } else if (bp.isMimeType("text/html")) {
      eml.setHtml(bp.getContent().toString());
    } else if (bp.isMimeType("text/plain")) {
      eml.setPlainText(bp.getContent().toString());
    } else if (bp.isMimeType("multipart/alternative") || bp.isMimeType("multipart/related")) {// si el body es una multiparte tenemos que ver que tiene
      EML.handleContent(eml, (MimeMultipart) bp.getContent());
    } else {//  cualquier otra cosa es un fichero adjunto
      InputStream is = (InputStream) bp.getContent();
      Attachment att = new Attachment(bp.getFileName());
      att.setContent(is);
      eml.getAttachments().add(att);
    }
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }
  
  public String getPlainText() {
    return plainText;
  }

  public void setPlainText(String plainText) {
    this.plainText = plainText;
  }

  public List<String> getRecipients() {
    return recipients;
  }
  
  public List<String> getRecipientsBcc() {
    return recipientsBcc;
  }

  public void setRecipients(List<String> recipients) {
    this.recipients = recipients;
  }
  
  public void setRecipientsBcc(List<String> recipientsBcc) {
    this.recipientsBcc = recipientsBcc;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public List<Attachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<Attachment> attachments) {
    this.attachments = attachments;
  }

  public List<EmbeddedImage> getEmbeddedImages() {
    return embeddedImages;
  }

  public void setEmbeddedImages(List<EmbeddedImage> embeddedImages) {
    this.embeddedImages = embeddedImages;
  }

  public MimeMessage getMessage() {
    return message;
  }

  public void setMessage(MimeMessage message) {
    this.message = message;
  }

}
