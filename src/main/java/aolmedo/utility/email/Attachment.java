/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aolmedo.utility.email;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author AOlmedo
 */
public class Attachment {
    
    private String dir;
    private String basename;
    private String extension;
    private InputStream content;
    
    /**
     * Crea un objeto de tipo Attachment utilizado para los ficheros adjuntos 
     * de los correos.
     * 
     * @param dir Directorio del fichero
     * @param filename Nombre del fichero
     */
    public Attachment(String dir, String filename) {
        this.dir = dir;
        this.extension = FilenameUtils.getExtension(filename);
        this.basename = FilenameUtils.getBaseName(filename);
    }
    
    /**
     * Crea un objeto de tipo Attachment utilizado para los ficheros adjuntos 
     * de los correos.
     * 
     * @param filename Nombre del fichero
     */
    public Attachment(String filename) {
        this.extension = FilenameUtils.getExtension(filename);
        this.basename = FilenameUtils.getBaseName(filename);
    }
    
    /**
     * Crea una lista de objetos Attachment a partir de los ficheros indicados
     * 
     * @param fullPaths
     * @return 
     */
    public static List<Attachment> createArrayListFromArrayString(List<String> fullPaths) {
        List<Attachment> list = new ArrayList<>();
        fullPaths.forEach((fullPath) -> {
            list.add(new Attachment(FilenameUtils.getFullPath(fullPath), FilenameUtils.getName(fullPath)));
        });
        return list;
    }

    /**
     * Devuelve el path completo del adjunto
     * 
     * @return 
     */
    public String getFullPath() throws Exception {
        if (this.dir == null) {
            throw new Exception("Dir is not set.");
        }
        return this.dir + this.basename + "." + this.extension;
    }
    
    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
    
    public String getBasename() {
        return basename;
    }

    public void setBasename(String basename) {
        this.basename = basename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }
    
}
