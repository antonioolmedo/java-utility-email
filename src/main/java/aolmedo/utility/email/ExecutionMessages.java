/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aolmedo.utility.email;

/**
 *
 * @author AOlmedo
 */
public class ExecutionMessages {
    
    private static final int EXECUTION_OK = 1;
    private static final int EXECUTION_KO = 0;
    
    public static void successMessage() {
        System.out.println(EXECUTION_OK);
    }
    
    public static void errorMessage() {
        System.out.println(EXECUTION_KO);
    }
    
}
