package aolmedo.utility.email;

import java.io.File;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEML {

  private final String subject = "Test subject";
  private final String from = "info@aolmedo.com";
  private final String to = "dev@aolmedo.com, info@aolmedo.com";
  private final String toBcc = "dev@aolmedo.com, info@aolmedo.com";

  @Test
  public void testACreate() throws Exception {
    //  creamos el fichero EML usando los ficheros de ejemplo
    EML eml = new EML(
            subject,
            from,
            to,
            "test/files/body.html",
            "test/files/bulb-2287759_640.jpg, "
            + "test/files/excel.xlsx, "
            + "test/files/word.docx",
            toBcc
    );
    eml.create("test/generated/generated");

    //  comprobamos que se ha creado el fichero
    File file = new File("test/generated/generated.eml");
    assertTrue(file.exists());
  }
  
  @Test
  public void testBSplit() throws Exception {
    EML eml = EML.createFromExisting("test/generated/generated.eml");
    String path = "test/generated" + File.separator;
    EML.split(path, eml);
      
    //  comprobamos que se ha creado el fichero
    File file = new File("test/generated/body.html");
    assertTrue(file.exists());
    file = new File("test/generated/bulb-2287759_640.jpg");
    assertTrue(file.exists());
    file = new File("test/generated/excel.xlsx");
    assertTrue(file.exists());
    file = new File("test/generated/word.docx");
    assertTrue(file.exists());
  }

  @Test
  public void testCInfo() throws Exception {
    JsonObjectBuilder job = Json.createObjectBuilder();
    EML eml = EML.createFromExisting("test/generated/generated.eml");
    JsonArrayBuilder jab = Json.createArrayBuilder();
    for (String recipient : eml.getRecipients()) {
      jab.add(Json.createObjectBuilder().add("email", recipient));
    }
    
    JsonArrayBuilder jab2 = Json.createArrayBuilder();
    for (String recipient : eml.getRecipientsBcc()) {
      jab2.add(Json.createObjectBuilder().add("email", recipient));
    }

    job.add("recipients", jab)
      .add("recipientsBcc", jab2)
      .add("subject", eml.getSubject())
      .add("from", eml.getFrom());

    String output = "{\"recipients\":[{\"email\":\"dev@aolmedo.com\"},{\"email\""
            + ":\"info@aolmedo.com\"}],\"recipientsBcc\":[{\"email\":\"dev@aolmedo.com\"},{\"email\""
            + ":\"info@aolmedo.com\"}],\"subject\":\"Test subject\",\"from\":"
            + "\"info@aolmedo.com\"}";

    assertEquals(job.build().toString(), output);
  }
}
