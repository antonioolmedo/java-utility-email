package aolmedo.utility.email;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestAttachment {
  @Test
	public void testCreate() throws Exception {
    Attachment atch = new Attachment("file.eml");
    
    assertEquals(atch.getBasename(), "file");
    assertEquals(atch.getExtension(), "eml");
    assertEquals(atch.getDir(), null);
    
    atch.setDir("/var/www/temp/");
    assertEquals(atch.getDir(), "/var/www/temp/");
    assertEquals(atch.getFullPath(), "/var/www/temp/file.eml");
	}
  
  @Test(expected=Exception.class)
	public void testFullPathWithoutDir() throws Exception {
    Attachment atch = new Attachment("file.eml");
    assertEquals(atch.getDir(), null);
    atch.getFullPath();
	}
}
