[![Build Status](https://travis-ci.org/antonioolmedo/java-utility-email.svg?branch=master)](https://travis-ci.org/antonioolmedo/java-utility-email)

# Email Utility
An utility for create/read an EML file on command line.

# Compile
`mvn clean compile assembly:single`

# Creator
Creates an EML file.

## How to use
`java -cp UtilityEmail.jar aolmedo.utility.email.executables/Creator {filename} {subject} {from} {recipients} {htmlFilename} {attachments}`

## Parameters:
- **filename**: path where EML file will save without extension ("C:\message").
- **subject**: Subject of EML ("Test message").
- **from**: From of EML ("info@aolmedo.com").
- **recipients**: Recipients separated by commas ("name@host.com, name2@host.com").
- **htmlFilename**: File where app read content for EML ("C:\body.html").
- **attachments**: Paths with files to attach ("C:\attc1.pdf, C:\attc2.xls").

# Info
Get JSON information about an EML file.

## How to use
`java -cp UtilityEmail.jar aolmedo.utility.email.executables/Info {filename}`

## Parameters:
- **filename**: Path of file to read ("C:\message.eml").

# Splitter
Read an EML file and writes body and attachments on a directory.

## How to use
`java -cp UtilityEmail.jar aolmedo.utility.email.executables/Info {filename} {dir}`

## Parameters:
- **filename**: Path of file to read ("C:\message.eml").
- **dir**: Path to write body and attachments ("C:\info-folder").
